# ==================
# Activity
# =================
# 1. In s03 folder, create an activity folder and an activity.py file inside the activity folder.
# 2. Create a graph class that has properties and methods for adding and printing edges.
# 3. Add a method in the class that will demonstrate BFS traversal

import time
from collections import deque

class Graph:
    def __init__(self, num_vertices):
        self.num_vertices = num_vertices
        self.adjacency_list = [[] for _ in range(num_vertices)]
        self.weights = [[0] * num_vertices for _ in range(num_vertices)]


    def add_edge(self, u, v, weight):
        self.adjacency_list[u].append(v)
        self.adjacency_list[v].append(u)
        self.weights[u][v] = weight
        self.weights[v][u] = weight

    def print_graph(self):
        for vertex in range(self.num_vertices):
            print(f"{chr(ord('P') + vertex)}: {', '.join(chr(ord('P') + v) for v in self.adjacency_list[vertex])}")

    def bfs(self, start_vertex):
        visited = [False] * self.num_vertices
        queue = deque()

        #1: Enqueue the start vertex and mark it as visited
        queue.append(start_vertex)
        visited[start_vertex] = True

        while queue:
            #2: Dequeue a vertex from the queue
            vertex = queue.popleft()
            print(chr(ord('P') + vertex), end=' ')
            time.sleep(1)  # Introduce a delay of 1 second

            #3: Enqueue all adjacent vertices of the dequeued vertex that are not visited
            for adjacent_vertex in self.adjacency_list[vertex]:
                if not visited[adjacent_vertex]:
                    queue.append(adjacent_vertex)
                    visited[adjacent_vertex] = True

    def bfs_target_search(self, start_vertex, target_vertex):
        visited = [False] * self.num_vertices
        queue = deque()

        queue.append(start_vertex)
        visited[start_vertex] = True

        while queue:
            vertex = queue.popleft()
            if vertex == target_vertex:
                return True
            for adjacent_vertex in self.adjacency_list[vertex]:
                if not visited[adjacent_vertex]:
                    queue.append(adjacent_vertex)
                    visited[adjacent_vertex] = True

        return False

    def dfs(self, start_vertex):
        visited = [False] * self.num_vertices
        self._dfs_helper(start_vertex, visited)

    def _dfs_helper(self, vertex, visited):
        visited[vertex] = True
        print(chr(ord('P') + vertex), end=' ')
        time.sleep(1)  # Introduce a delay of 1 second

        for adjacent_vertex in self.adjacency_list[vertex]:
            if not visited[adjacent_vertex]:
                self._dfs_helper(adjacent_vertex, visited)


# 4. Create a list of nodes and store it inside a variable.

nodes = ['P', 'S', 'I', 'R', 'Q', 'T']
graph1 = Graph(len(nodes))


# 5. Instantiate the graph class and pass the list of nodes as arguments.

graph1.add_edge(0, 1, 2)  # P -> S (weight 2)
graph1.add_edge(0, 2, 1)  # P -> I (weight 1)
graph1.add_edge(0, 3, 3)  # P -> R (weight 3)
graph1.add_edge(2, 4, 4)  # I -> Q (weight 4)
graph1.add_edge(3, 4, 2)  # R -> Q (weight 2)
graph1.add_edge(3, 5, 1)  # R -> T (weight 1)

# - Add edges to the graph, make sure that the graph to be created is a directed, weighted, connected graph.
# 6. Print all the edges of the graph

print("Graph:")
graph1.print_graph()

# 7. Execute the bfs traversal on the instance created.
print("\nBFS traversal:")
graph1.bfs(0)

# 8. Add another method in the class to demonstrate bfs target node search


# 9. Create a list of nodes and store it inside a variable
nodes2 = ['P', 'S', 'I', 'R', 'Q', 'T']
graph2 = Graph(len(nodes))

graph2.add_edge(0, 1, weight=2)  # P - S (weight 2)
graph2.add_edge(1, 2, weight=3)  # S - I (weight 3)
graph2.add_edge(3, 4, weight=4)  # R - Q (weight 4)
graph2.add_edge(4, 5, weight=1)  # Q - T (weight 1)

print("\nDFS Traversal:")
dfs_path = graph2.dfs(0)

# 10. Create another instance of the graph class
# - Pass the list of nodes as arguments.
# - Add edges to the graph, make sure that the graph to be created is an undirected, weighted disconnected graph
# 11. Execute the BFS target node search and look for the nodes that are not connected be any edge.
# 	- Create a path array variable to store the path.
# - Reassign the value of the path and store the return value given by the BFS target node search
# 	- Print the value of the path

print("\n\nBFS Target Node Search:")
target_node = 2
is_reachable = graph2.bfs_target_search(0, target_node)

if is_reachable:
    print(f"{chr(ord('P') + target_node)} is reachable from P")
else:
    print(f"{chr(ord('P') + target_node)} is not reachable from P")


# 12. Add another method in the class to demonstrate dfs
# 13. Create a list of nodes and store it inside a variable
nodes3 = ['P', 'S', 'I', 'R', 'Q', 'T']
graph3 = Graph(len(nodes))

# Adding edges to the graph (directed, weighted, and connected)
graph3.add_edge(0, 1, weight=2)  # P -> S (weight 2)
graph3.add_edge(0, 2, weight=1)  # P -> I (weight 1)
graph3.add_edge(0, 3, weight=3)  # P -> R (weight 3)
graph3.add_edge(2, 4, weight=4)  # I -> Q (weight 4)
graph3.add_edge(3, 4, weight=2)  # R -> Q (weight 2)
graph3.add_edge(3, 5, weight=1)  # R -> T (weight 1)

print("\nDFS Traversal:")
dfs_path = graph3.dfs(0)

# 14. Create another instance of the graph class
# 15. Instantiate the graph class and pass the list of nodes as arguments.
# - Add edges to the graph, make sure that the graph to be created is an directed, weighted, connected graph
# 16. Execute the DFS method
# 	- Create a path array variable to store the path.
# 	- Reassign the value of the path and store the return value given by the DFS
# 	- Print the value of the path
# 17. Create a git repository named S03. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. Add the link in Boodle.


