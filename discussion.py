# Discussion points
# ==========================
# Graph Traversal
# 	- refers to the process of visiting all the vertices or nodes of graph in a systematic way. It involves exploring or traversig the graph to access its various elements (nodes and edges)
# 		- nodes(vertices)
# 			- represent the entries or points in graph
# 		- edges
# 			- are connections or relationships between nodes in graph
# Two common methods:
# 	- Breadth-First Search(BFS)
# 		- graph traversal algorithm that explores all the vertices of graph in breadth-first order, starting from the selected vertex called 'source' or 'starting vertex'
#     - Breadth-First Search (BFS) traverses the graph systematically, level by level, forming a BFS Tree along the way.
# 			It is one of the fundamental search  algorithm used to explore nodes and edges of a graph.
      
#       -- A standard BFS implementation puts each vertex of the graph into one of two categories:
#       1. Visited
#       2. Not Visited
#       The purpose of the algorithm is to mark each vertex as visited while avoiding cycles.


# 	- Depth-First Search(DFS)
#   	- Depth-First Search (DFS) is a simple yet useful algorithm that used to traverse or locate a target node in a graph or tree data structure. 
#     - It priorities depth and searches along one branch, as far as it can go - until the end of that branch. Once there, it backtracks to the first possible divergence from that branch, 
#     	and searches until the end of that branch, repeating the process.
      
#       -- The start node is the root node for tree data structures, while with more generic graphs - it can be any node.

#   	There are only few steps for its process.
#       Below are the steps on its Algorithm:
#       1. 	We will start by putting starting node in the graph on top of the stack.
#       2. 	Mark the node as visited by adding it to the visited nodes list.
#       3. Traverse the neighboring nodes that aren't visited.
#       4. 	Once the node has no neighbor node, it will pop out from our stack 
#       and the process continue to find the unvisited nodes from other 
#       nodes.
#       5. 	The algorithm stops either when the target node is found, or the 
#       whole graph has been traversed.


  
# ==========================
import time
from collections import deque

class Graph:
    def __init__(self, num_vertices):
        self.num_vertices = num_vertices
        self.adjacency_list = [[] for _ in range(num_vertices)]

    def add_edge(self, u, v):
        self.adjacency_list[u].append(v)
        self.adjacency_list[v].append(u)

    def print_graph(self):
        for vertex in range(self.num_vertices):
            print(f"{chr(ord('P') + vertex)}: {', '.join(chr(ord('P') + v) for v in self.adjacency_list[vertex])}")

    def bfs(self, start_vertex):
        visited = [False] * self.num_vertices
        queue = deque()

        #1: Enqueue the start vertex and mark it as visited
        queue.append(start_vertex)
        visited[start_vertex] = True

        while queue:
            #2: Dequeue a vertex from the queue
            vertex = queue.popleft()
            print(chr(ord('P') + vertex), end=' ')
            time.sleep(1)  # Introduce a delay of 1 second

            #3: Enqueue all adjacent vertices of the dequeued vertex that are not visited
            for adjacent_vertex in self.adjacency_list[vertex]:
                if not visited[adjacent_vertex]:
                    queue.append(adjacent_vertex)
                    visited[adjacent_vertex] = True


# Create the graph
graph = Graph(6)

#1: Add edges to the graph
graph.add_edge(0, 1)  # P - S
graph.add_edge(0, 2)  # P - I
graph.add_edge(0, 3)  # P - R
graph.add_edge(2, 4)  # I - Q
graph.add_edge(3, 4)  # R - Q
graph.add_edge(3, 5)  # R - T

#2: Print the graph
print("Graph:")
graph.print_graph()

#3: Perform BFS starting from vertex P
print("\nBFS traversal:")
graph.bfs(0)

# =========================
# dfs2.py
# =======================
import time

class Graph:
    def __init__(self, num_vertices):
        self.num_vertices = num_vertices
        self.adjacency_list = [[] for _ in range(num_vertices)]

    def add_edge(self, u, v, weight):
        self.adjacency_list[u].append(v)
        self.adjacency_list[v].append(u)


    def print_graph(self):
        for vertex in range(self.num_vertices):
            print(f"{chr(ord('P') + vertex)}: {', '.join(chr(ord('P') + v) for v in self.adjacency_list[vertex])}")

    def dfs(self, start_vertex):
        visited = [False] * self.num_vertices
        self._dfs_helper(start_vertex, visited)

    def _dfs_helper(self, vertex, visited):
        visited[vertex] = True
        print(chr(ord('P') + vertex), end=' ')
        time.sleep(1)  # Introduce a delay of 1 second

        for adjacent_vertex in self.adjacency_list[vertex]:
            if not visited[adjacent_vertex]:
                self._dfs_helper(adjacent_vertex, visited)


# Create the graph
graph = Graph(6)

# Step 1: Add edges to the graph
graph.add_edge(0, 1)  # P - S
graph.add_edge(0, 2)  # P - I
graph.add_edge(0, 3)  # P - R
graph.add_edge(2, 4)  # I - Q
graph.add_edge(3, 4)  # R - Q
graph.add_edge(3, 5)  # R - T

# Step 2: Print the graph
print("Graph:")
graph.print_graph()

# Step 3: Perform DFS starting from vertex P
print("\nDFS traversal:")
graph.dfs(0)


# ==================
# Activity
# =================
# 1. In s03 folder, create an activity folder and an activity.py file inside the activity folder.
# 2. Create a graph class that has properties and methods for adding and printing edges.
# 3. Add a method in the class that will demonstrate BFS traversal
# 4. Create a list of nodes and store it inside a variable.
# 5. Instantiate the graph class and pass the list of nodes as arguments.
# - Add edges to the graph, make sure that the graph to be created is a directed, weighted, connected graph.
# 6. Print all the edges of the graph
# 7. Execute the bfs traversal on the instance created.
# 8. Add another method in the class to demonstrate bfs target node search
# 9. Create a list of nodes and store it inside a variable
# 10. Create another instance of the graph class
# - Pass the list of nodes as arguments.
# - Add edges to the graph, make sure that the graph to be created is an undirected, weighted disconnected graph
# 11. Execute the BFS target node search and look for the nodes that are not connected be any edge.
# 	- Create a path array variable to store the path.
# - Reassign the value of the path and store the return value given by the BFS target node search
# 	- Print the value of the path
# 12. Add another method in the class to demonstrate dfs
# 13. Create a list of nodes and store it inside a variable
# 14. Create another instance of the graph class
# 15. Instantiate the graph class and pass the list of nodes as arguments.
# - Add edges to the graph, make sure that the graph to be created is an directed, weighted, connected graph
# 16. Execute the DFS method
# 	- Create a path array variable to store the path.
# 	- Reassign the value of the path and store the return value given by the DFS
# 	- Print the value of the path
# 17. Create a git repository named S03. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. Add the link in Boodle.


